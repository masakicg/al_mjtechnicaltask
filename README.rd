
Objective:

In Python or C++, write a command line tool which shows how you would take some sets of personal data (name, address, phone number) and serialize them/de-serialize them in at least 2 formats, and display it in at least 2 different ways (no need to use a GUI Framework - text output/HTML or any other human readable format is  fine).  There is no need to support manual data entry - you could manually write a file in one of your chosen formats to give you your input test data.

Write it in such a way that it would be easy for a developer:
to add support for additional storage formats
to query a list of currently supported formats
to supply an alternative reader/writer for one of the supported formats

This should ideally show Object-Oriented Design and Design Patterns Knowledge, we’re not looking for use of advanced Language constructs. Provide reasonable Unit Test coverage.

Due by: 10:00am Friday, August 18th, 2017


How to use:

Please run the main module to use: AL_mjTechnicalTask.py
Directions for running via python on the command line (taken from AL_mjTechnicalTask.py --h):
'Optional argument for .json/.pickle files can be provided. Otherwise, a default example file is used.'

Unit tests can be ran directly from tests/tests.py in the project.