#!/usr/bin/env python

'''
Unit test for each class in AL_mjTechnicalTaskLib.AL_mjTechnicalTaskClasses
'''

# September 2017 Masaki Jeffrey for Animal Logic

# Import built-in modules
import os, re, sys, unittest

# Add the project directory to the python system path.
project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if project_dir not in sys.path:
    sys.path.append(project_dir)

# Import AL_mjTechnicalTask modules
from AL_mjTechnicalTaskLib.AL_mjTechnicalTaskClasses import Contact, ContactSheet
from AL_mjTechnicalTaskLib.AL_mjTechnicalTaskIO import *


class TestContact(unittest.TestCase):
    '''Unit tests for Contact class'''
    DEFAULT_KWARGS = {
        "address": {
            "city": "Vancouver",
            "country": "Canada",
            "postal_code": "V1V 1V1",
            "province": "BC",
            "street": "123 4th Street, Suite 5"
        },
        "name": {
            "first": "John",
            "last": "Doe"
        },
        "phone_numbers": {
            "home": "604-555-0000",
            "mobile": "1-604-555-1111",
            "work": "(604)555-2222"
        }
    }

    def setUp(self):
        '''Contact instance created with default keyword arguments, available for instance testing.'''
        self.Contact = Contact(**self.DEFAULT_KWARGS)

    def test_constructor(self):
        '''Check arguments passed to the class constructor.'''
        # Keyword arguments must match the expected parameters.
        self.assertItemsEqual(Contact.FIELDS, self.DEFAULT_KWARGS.keys())
        self.assertItemsEqual(('street', 'city', 'province', 'country', 'postal_code'), self.DEFAULT_KWARGS['address'].keys())
        self.assertItemsEqual(('first', 'last'), self.DEFAULT_KWARGS['name'].keys())

    def test_get_data_copy(self):
        '''Ensure that copies of dictionaries are returned, so that pointers are not sharing the same data.'''
        contact_as_dict = self.Contact.to_dict()
        self.assertIsNot(contact_as_dict['address'], self.Contact._address)
        self.assertIsNot(contact_as_dict['name'], self.Contact._name)
        self.assertIsNot(contact_as_dict['phone_numbers'], self.Contact._phone_numbers)


class TestContactSheet(unittest.TestCase):
    '''Unit tests for ContactSheet class.'''
    DEFAULT_KWARGS = parse_args()

    def setUp(self):
        '''ContactSheet instance created with default keyword arguments, available for instance testing.'''
        self.ContactSheet = ContactSheet(**self.DEFAULT_KWARGS)

    def test_constructor(self):
        '''Check arguments passed to the class constructor.'''
        # Ensure that the path arguments exist.
        self.assertTrue(os.path.isdir(self.DEFAULT_KWARGS['project_dir']))
        self.assertTrue(os.path.isfile(self.DEFAULT_KWARGS['file_path']))

    def test_add_contact(self):
        '''
        Check that missing keys for name and address objects give errors when creating a contact,
        but not phone_numbers, which can have any number of entries.
        '''
        n = {'first': 'Test', 'last': 'Test'}
        a = {'street': 'Test Ave', 'city': 'Testville', 'province': 'TEST', 'country': 'Test', 'postal_code': 'T1T1T1'}
        p_n = {'home': '5555555'}
        with self.assertRaises(KeyError):
            error_n = n.copy()
            del error_n['last']
            self.ContactSheet.add_contact(name=error_n, address=a, phone_numbers=p_n)
        with self.assertRaises(KeyError):
            error_a = a.copy()
            del error_a['street']
            self.ContactSheet.add_contact(name=error_n, address=a, phone_numbers=p_n)
        # If successful, the add_contact call will return a value.
        empty_p_n = {}
        self.assertTrue(self.ContactSheet.add_contact(name=n, address=a, phone_numbers=empty_p_n))

    def test_class_methods(self):
        '''Check methods for ContactSheet.'''
        # Create a list of all ContactSheet public methods.
        funcs = filter(lambda attr: callable(getattr(ContactSheet, attr)) and re.match('^(?!_)\w+', attr), dir(ContactSheet))
        # Check that each method has a proper docstring.
        self.assertTrue(all(getattr(ContactSheet, func).__doc__ for func in funcs))

    def test_load_contacts(self):
        '''Check that the contact sheet populates correctly when desalinizing information from a file'''
        # Check that an error is raised if the file_path is not a file and/or does not exist,
        with self.assertRaises(IOError):
            self.ContactSheet.load_contacts_from_file('_.json')
        # Check that the path is not case sensitive
        self.assertTrue(self.ContactSheet.load_contacts_from_file(self.DEFAULT_KWARGS['file_path'].upper()))

    def test_write_contacts(self):
        '''Check that the contact sheet can be exported with all human-readable output formats.'''
        self.assertTrue(self.ContactSheet.write_contacts_all(False))


if __name__ == '__main__':
    unittest.main()