#!/usr/bin/env python

# Import built-in modules
import os, sys

# Add the project directory to the python system path.
project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if project_dir not in sys.path:
    sys.path.append(project_dir)