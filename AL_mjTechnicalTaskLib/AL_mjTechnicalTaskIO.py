#!/usr/bin/env python

# September 2017 Masaki Jeffrey for Animal Logic

# Import built-in modules
from argparse import ArgumentParser
from ConfigParser import SafeConfigParser
import os, sys


def check_dir_path(func, make_dirs=True):
    '''
    Wrapper function to check if the argument, 'file_path' is an existing directory.
    If it does not exist, and make_dirs is True,
    the directory tree is automatically created before running the decorated function.
    This behaviour is only executed if file_path is passed as a keyword argument, to avoid issues with order when passed through args.
    '''
    def wrapped_func(*args, **kwargs):
        '''If file_path is in a directory that doesn't exist, the directory tree is created.'''
        dir_path = os.path.dirname(kwargs.get('file_path', ''))
        if all((make_dirs, dir_path, not os.path.isdir(dir_path))):
            os.makedirs(dir_path)
        return func(*args, **kwargs)

    return wrapped_func


def parse_args():
    '''
    Generate default and user defined keyword arguments used in AL_mjTechnicalTask.main() to create an instance of ContactSheet.
    '''
    # Create a dictionary to hold keyword arguments to be used in instantiating ContactSheet.
    contact_sheet_kwargs = dict()

    # Load default project settings into contact_sheet_kwargs
    config_parser = SafeConfigParser()
    config_parser.read('defaults.ini')
    contact_sheet_kwargs['contact_sheet_name'] = config_parser.get('project', 'contact_sheet_name')
    contact_sheet_kwargs['project_dir'] = eval(config_parser.get('project', 'project_dir'))
    example_file = config_parser.get('project', 'example_file')
    default_file_path = os.path.realpath(os.path.join(contact_sheet_kwargs['project_dir'], example_file))

    # If the user passed a file path as an argument, override the default file path of the serialized file.
    parser_help = 'Optional argument for .json/.pickle files can be provided. Otherwise, a default example file is used.'
    file_path_help= 'Provide the file path for a previously serialozed contact sheet.'
    arg_parser = ArgumentParser(epilog=parser_help)
    arg_parser.add_argument('file_path', default=default_file_path, nargs='?', help=file_path_help)
    cmd_args = arg_parser.parse_args()
    file_path = cmd_args.file_path if cmd_args.file_path else default_file_path

    # Check that the serialized file exists.
    contact_sheet_kwargs['file_path'] = file_path if os.path.isfile(file_path) else ''
    # If all the keys have values in contact_sheet_kwargs, return contact_sheet_kwargs
    return contact_sheet_kwargs if all(contact_sheet_kwargs.values()) else None


def start_file(file_path=''):
    '''Open the given file with the default application set by the user's platform'''
    platform = sys.platform
    # On Windows
    if platform == 'win32':
        os.startfile(file_path)
    # On Linux
    elif platform in ('linux', 'linux2'):
        os.system('xdg-open "{0}"'.format(file_path))
    # On Mac
    elif platform in ('darwin', 'os2'):
        os.system('open "{0}"'.format(file_path))
    # On Windows, using cygwin
    elif platform == 'cygwin':
        os.system('cygstart "{0}"'.format(file_path))
    # ???
    else:
        return None


class FileIO(object):
    '''Convenience class for reading and writing files.'''
    def read(self, file_path):
        '''Read the contents of the given file path and return the contents as a string.'''
        with open(file_path, 'r') as read_file:
            serialized_data = read_file.read()
            return serialized_data

    @check_dir_path
    def write(self, file_path, serialized_data):
        '''Write the data as a string to the given file path.'''
        with open(file_path, 'w') as write_file:
            write_file.write(str(serialized_data))
            return True