#!/usr/bin/env python

# September 2017 Masaki Jeffrey for Animal Logic

#Import built-in modules
import copy, cPickle, csv, json, os, re
from collections import namedtuple
from StringIO import StringIO

# Import IO module
from AL_mjTechnicalTaskIO import *


# REGISTRY CLASSES


RegEntry = namedtuple('RegEntry', ('cls', 's_formats'))


class Registry(object):
    '''
    Object to register all Serializer classes.
    Access registry with class attribute: Registry._cls_registry.
    The registered classes are queried when working with serialized data for different encoding formats.
    The registered class must have a list of compatible formats given in the class attribute: _s_formats.
    '''
    _cls_registry = dict()

    @classmethod
    def register_class(cls, reg_cls):
        '''Register the given class to Registry._cls_registry'''
        cls._cls_registry[reg_cls.__name__] = RegEntry(cls=reg_cls, s_formats=reg_cls._s_formats)


class MetaRegister(type):
    '''Metaclass for registering all inheriting classes.'''
    def __new__(meta, name, bases, class_dict):
        '''
        Automatically register the inheriting class to Registry.
        The registered class must have a list of compatible formats given in the class attribute: _s_formats.
        '''
        cls = type.__new__(meta, name, bases, class_dict)
        if bases != (object,):
            if not class_dict.get('_s_formats', list()):
                raise ValueError('Class method: _s_formats must be set with at least one valid format.')
        Registry.register_class(cls)
        return cls


# SERIALIZER CLASSES


class Serializer(object):
    '''
    Base class to be inherited by serializing classes. Do NOT instantiate this class directly.
    In the sub-class:
    >Provide a list of file extensions for the class attribute: _s_formats.
    >Re-implement the deserialize() and serialize() methods in the sub-class.
    '''
    __metaclass__ = MetaRegister
    _s_formats = list()

    @staticmethod
    def get_class(*args):
        '''
        Search the Registry for a serializer class for any of the s_formats formats in args.
        Return the first class that fits the criteria; otherwise, return None
        '''
        for arg in args:
            for reg_entry in Registry._cls_registry.values():
                if arg in reg_entry.s_formats:
                    return reg_entry.cls
        return None

    @staticmethod
    def get_s_formats():
        '''Return a list of all serialization formats currently registered through Serializer classes.'''
        return sum([reg_entry.s_formats for reg_entry in Registry._cls_registry.values()], list())

    def deserialize(self, serialized_data):
        '''Deserialize the serialized data. This must be reimplemented in the sub-class.'''
        raise NotImplementedError('The deserialize() method must be set for {0}'.format(self.__class__.__name__))

    def serialize(self, deserialized_data):
        '''Serialize the serialized data. This must be reimplemented in the sub-class.'''
        raise NotImplementedError('The serialize() method must be set for {0}'.format(self.__class__.__name__))


class SerializerCSV(Serializer):
    '''Extension of the Serializer class to serialize CSV data.'''
    _s_formats = ['csv']

    def deserialize(self, serialized_data):
        '''Reimplementation of the serialize method to load CSV data.'''
        deserialized_data = list()
        csv_reader = csv.DictReader(StringIO(serialized_data))
        for row in csv_reader:
            data = dict()
            for k, v in row.items():
                if isinstance(v, str):
                    data[k] = eval(v)
            deserialized_data.append(data)
        return deserialized_data

    def serialize(self, deserialized_data):
        '''Reimplementation of the serialize method to dump CSV data.'''
        serialized_data = StringIO()
        csv_writer = csv.DictWriter(
            serialized_data,
            fieldnames = Contact.FIELDS,
            quoting = csv.QUOTE_MINIMAL
        )
        csv_writer.writeheader()
        csv_writer.writerows(deserialized_data)
        return serialized_data.getvalue().strip('\r\n')


class SerializerJSON(Serializer):
    '''Extension of the Serializer class to serialize JSON data.'''
    _s_formats = ['json']

    def deserialize(self, serialized_data):
        '''Reimplementation of the serialize method to load JSON data.'''
        deserialized_data = json.loads(serialized_data)
        return deserialized_data['data']

    def serialize(self, deserialized_data):
        '''Reimplementation of the serialize method to dump JSON data.'''
        return json.dumps({
            'cls': self.__class__.__name__,
            'data': deserialized_data
            },
            indent = 4,
            sort_keys = True
        )


class SerializerPickle(Serializer):
    '''Extension of the Serializer class to serialize Pickle data.'''
    _s_formats = ['pickle']

    def deserialize(self, serialized_data):
        '''Reimplementation of the serialize method to load Pickle data.'''
        deserialized_data = cPickle.loads(serialized_data)
        return deserialized_data['data']

    def serialize(self, deserialized_data):
        '''Reimplementation of the serialize method to dump Pickle data.'''
        return cPickle.dumps({
            'cls': self.__class__.__name__,
            'data': deserialized_data
        })


# Add additional Serializer classes here...


# CONTACT SHEET CLASSES


class Contact(object):
    '''Contact object for a single contact that holds information in three fields (see Contact.FIELDS).'''
    FIELDS = ('name', 'address', 'phone_numbers')

    def __init__(self, name={}, address={}, phone_numbers={}):
        '''
        Initialize contact info with the following dictionary object args:
        name = {'first': '', 'last': ''}
        address = {'street': '', 'city': '', 'province': '', 'country': '', 'postal_code': ''}
        phone_numbers = {{'<alias>': '<number>'}, ...}
        '''
        super(Contact, self).__init__()
        self._name = copy.deepcopy(name)
        self._address = copy.deepcopy(address)
        self._phone_numbers = copy.deepcopy(phone_numbers)

    def __str__(self):
        '''Return a string representation of all the contact's info.'''
        first_name, last_name = self._name['first'], self._name['last']
        address = self.get_address()
        phone_numbers = self.get_phone_numbers()
        return 'First Name: {0}\nLast Name: {1}\n\nAddress:\n{2}\n\nPhone Numbers:\n{3}'.format(
            first_name,
            last_name,
            address,
            phone_numbers
        )

    def get_address(self, section=None):
        '''
        Get a string representation of the contact's address.
        If a section (street, city, province, country, postal_code) is given, only information for that section is returned.
        Otherwise, information for all sections is returned.
        '''
        if section:
            return self._address[section]
        else:
            address = '{0}\n{1}, {2}\n{3}\n{4}'.format(
                self._address['street'],
                self._address['city'],
                self._address['province'],
                self._address['country'],
                self._address['postal_code']
            )
            return address

    def get_full_name(self):
        '''
        Get a string representation of the contact's full name - last name then first name.
        This will be identical to the key used in ContactSheet for this contact.
        '''
        full_name = '{0}, {1}'.format(self._name['last'], self._name['first'])
        return full_name

    def get_phone_numbers(self):
        '''Get a string representation of all the contact's phone numbers.'''
        phone_numbers_list = ['{0}: {1}'.format(k, v) for k, v in self._phone_numbers.items()]
        phone_numbers = '\n'.join(sorted(phone_numbers_list))
        return phone_numbers

    def remove_phone_number(self, alias):
        '''Remove the contact's phone number of the given alias.'''
        if alias in self._phone_numbers.keys():
            del self._phone_numbers[key]
            return True
        return False

    def to_dict(self):
        '''
        Return the Contact's address, name, and phone numbers as a dictionary of dictionaries.
        Used when serializing contact information.
        '''
        contact_dict = dict()
        contact_dict['address'] = copy.deepcopy(self._address)
        contact_dict['name'] = copy.deepcopy(self._name)
        contact_dict['phone_numbers'] = copy.deepcopy(self._phone_numbers)
        return contact_dict


class ContactSheet(object):
    '''Contact Sheet object for (de)serializing and working with a collection of Contacts.'''

    def __init__(self, project_dir, contact_sheet_name, s_format='json', file_path=None):
        '''Instantiate a new ContactSheet, which holds a hash of Contacts.'''
        super(ContactSheet, self).__init__()
        self._contacts = dict()
        self._fileIO = FileIO()

        self.contact_sheet_name = contact_sheet_name
        self.project_dir = project_dir
        self.s_format = s_format

        # If a file path is passed, attempt do deserialize the information from the file and create contacts.
        if file_path:
            self.load_contacts_from_file(file_path)

    # ContactSheet instance properties:

    @property
    def contact_sheet_name(self):
        '''The contact sheet name is used as the file name when writing files.'''
        return self._contact_sheet_name

    @contact_sheet_name.setter
    def contact_sheet_name(self, contact_sheet_name):
        '''Set the contact sheet name.'''
        self._contact_sheet_name = contact_sheet_name

    @property
    def project_dir(self):
        '''The project directory is the base directory where for reading and writing files.'''
        return self._project_dir

    @project_dir.setter
    def project_dir(self, project_dir):
        '''Set the project directory. Output files written will be saved in an 'output' sub-directory.'''
        if os.path.isdir(project_dir):
            self._project_dir = project_dir
            self._output_dir = os.path.join(project_dir, 'output')

    @property
    def s_format(self):
        '''Get the current serialization format for (de)serializing files.'''
        return self._s_format

    @s_format.setter
    def s_format(self, s_format):
        '''Set the serialization format and the serializer object.'''
        if s_format in Serializer.get_s_formats():
            self._s_format = s_format
            serializer_class = Serializer.get_class(s_format)
            if serializer_class:
                self._serializer = serializer_class()
            return True
        return False

    # ContactSheet instance methods:

    def add_contact(self, name, address, phone_numbers):
        '''
        Initialize contact info with the following dictionary object args:
        name = {'first': '', 'last': ''}
        address = {'street': '', 'city': '', 'province': '', 'country': '', 'postal_code': ''}
        phone_numbers = {{'<alias>': '<number>'}, ...}
        If the name already exists in the contact sheet, all information for that contact is overwritten.
        '''
        new_contact = Contact(name, address, phone_numbers)
        full_name = new_contact.get_full_name()
        if full_name in self._contacts:
            del self._contacts[full_name]
        self._contacts[full_name] = new_contact
        print 'New contact: {0} added!'.format(full_name)
        return full_name

    def get_contact_info(self, full_name, field=''):
        '''
        Given the contact's full name, get the given contact's information.
        If a field (address or phone_number) is given, a string representation of that information is returned.
        Otherwise, a string representation of all the contact's information is returned.
        '''
        assert full_name in self._contacts.keys()
        if field == 'address':
            return self._contacts[full_name].get_address()
        elif field == 'phone_numbers':
            return self._contacts[full_name].get_phone_numbers()
        else:
            return str(self._contacts[full_name])

    def list_contacts(self):
        '''Print and return a list of the full names for all contacts in the contact sheet.'''
        print self._contacts.keys()
        return self._contacts.keys()

    def load_contacts_from_file(self, file_path):
        '''
        Read and deserialize the previously exported Contact Sheet contents from the given file path.
        Rebuild the hash of contacts from this data.
        '''
        file_type = file_path.split('.')[-1]
        self.s_format = file_type
        serialized_data = self._fileIO.read(file_path=file_path)
        deserialized_data = self._serializer.deserialize(serialized_data)
        if deserialized_data:
            self.remove_contacts_all()
            for contact_info in deserialized_data:
                self.add_contact(**contact_info)
            print 'Contact sheet successfully loaded from:\n{0}'.format(file_path)
            return True

    def remove_contact(self, full_name):
        '''Given the contact's full name, remove the given contact from the contact sheet.'''
        if full_name in self._contacts.keys():
            del self._contacts[full_name]
            print '{0} has died and was removed from the contact sheet...'.format(full_name)
            return True
        return False

    def remove_contacts_all(self):
        '''
        Remove all contacts from the contact sheet.
        Basically resetting self._contacts, but removing the Contact objects explicitly to clear data.
        '''
        for full_name in self._contacts.keys():
            self.remove_contact(full_name)

    def write_contacts(self, open_when_done=True):
        '''
        Output all contacts to a file of the current display output file type.
        If open_when_done is True, the file will be opened by the user's default application.
        '''
        file_path = os.path.join(self._output_dir, '.'.join((self.contact_sheet_name, self.s_format)))
        contacts = [contact.to_dict() for contact in self._contacts.values()]
        serialized_data = self._serializer.serialize(contacts)
        if self._fileIO.write(file_path=file_path, serialized_data=serialized_data):
            print 'Contacts successfully written to:\n', file_path
            # Open the newly written file.
            if open_when_done:
                start_file(file_path)
            return True
        return False

    def write_contacts_all(self, open_when_done=True):
        '''Output all contacts to all display output file types.'''
        success = list()
        for s_format in Serializer.get_s_formats():
            self.s_format = s_format
            success.append(self.write_contacts(open_when_done))
        return all(success)