#!/usr/bin/env python

'''
Summary
>A ContactSheet object is used to add, remove, and change individual contacts and their information.
>Each contact in a ContactSheet is represented as a Contact object.
>Information for all contacts can be displayed and/or (de)serialized to external files using methods provided in the ContactSheet class.

To add a new serialization format:
>Update AL_mjTechnicalTaskClasses:
    >Import any necessary modules that will be used to serialize/deserialize data.
    >Extend the Serialzer base class to create new classes for the new format(s).
    >Provide a list of file extensions in the new sub-class' class attribute: _s_formats.
    >Re-implement the serialize() and deserialize() methods in these new sub-classes.
'''

# September 2017 Masaki Jeffrey for Animal Logic

# Import AL_mjTechnicalTask modules
from AL_mjTechnicalTaskLib.AL_mjTechnicalTaskClasses import ContactSheet
from AL_mjTechnicalTaskLib.AL_mjTechnicalTaskIO import parse_args


def main():
    '''
    Execute a demonstration of the script.
    The script will create a contact sheet instance,
    and then prompt the user to run through some methods sequentially.
    Details fo the process are commented below.
    '''

    # Provide some defaults arguments when creating the ContactSheet instance.
    contact_sheet_kwargs = parse_args()

    if contact_sheet_kwargs:
        # Create a new contact sheet.
        # Will deserialize from the default.json file found in ./examples to generate contacts by default, unless an argument was passed.
        contact_sheet = ContactSheet(**contact_sheet_kwargs)

        # List all contacts in the contact sheet.
        list_contacts_prompt = raw_input('list_contacts()') # User prompt
        contact_list = contact_sheet.list_contacts()

        # Serialize the contact sheet in all formats.
        # Will save files to ./output for the project.
        serialize_contacts_prompt = raw_input('serialize_contacts_all()') # User prompt
        # contact_sheet.write_contacts_all() # (Un)comment this statement to write and launch the files.
        contact_sheet.write_contacts_all(False) # (Un)comment this statement to write without launching the files.

        # Remove all contacts from the contact sheet.
        remove_contact_prompt = raw_input('remove_contacts_all()') ###
        contact_sheet.remove_contacts_all()

        # END
        del contact_sheet
    else:
        print 'Invalid file'

if __name__ == '__main__':
    main()